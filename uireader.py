from PyQt6.uic import loadUi
from PyQt6.QtWidgets import QApplication, QTableWidgetItem
from sqlshot import sqlqueryinsertrecord, sqlqueryselecttbl, sqlquerydeleterec


class Main:
    def __init__(self):
        self.mainui = loadUi('tablesearch.ui')
        self.mainui.show()
        
        self.mainui.chkApple.stateChanged.connect(self.funcapple)
        self.mainui.chkOrange.stateChanged.connect(self.funcorange)
        self.mainui.chkStarApple.stateChanged.connect(self.funcstarapple)
        self.mainui.chkBanana.stateChanged.connect(self.funcbanana)
        self.mainui.chkMango.stateChanged.connect(self.funcmango)
        self.mainui.chkGuava.stateChanged.connect(self.funcguava)
        self.mainui.chkGrapes.stateChanged.connect(self.funcgrapes)
        
    
    def funcapple(self):
        if self.mainui.chkApple.isChecked():
            sqlqueryinsertrecord("Apple")
            self.loadtbl()
        else:
            sqlquerydeleterec("Apple")
            self.loadtbl()
    
    def funcorange(self):
        if self.mainui.chkOrange.isChecked():
            sqlqueryinsertrecord("Orange")
            self.loadtbl()
        else:
            sqlquerydeleterec("Orange")
            self.loadtbl()
    
    def funcstarapple(self):
        if self.mainui.chkStarApple.isChecked():
            sqlqueryinsertrecord("Star Apple")
            self.loadtbl()
        else:
            sqlquerydeleterec("Star Apple")
            self.loadtbl()
    
    def funcbanana(self):
        if self.mainui.chkBanana.isChecked():
            sqlqueryinsertrecord("Banana")
            self.loadtbl()
        else:
            sqlquerydeleterec("Banana")
            self.loadtbl()
    
    def funcmango(self):
        if self.mainui.chkMango.isChecked():
            sqlqueryinsertrecord("Mango")
            self.loadtbl()
        else:
            sqlquerydeleterec("Mango")
            self.loadtbl()
            
    def funcguava(self):
        if self.mainui.chkGuava.isChecked():
            sqlqueryinsertrecord("Guava")
            self.loadtbl()
        else:
            sqlquerydeleterec("Guava")
            self.loadtbl()
    
    def funcgrapes(self):
        if self.mainui.chkGrapes.isChecked():
            sqlqueryinsertrecord("Grapes")
            self.loadtbl()
        else:
            sqlquerydeleterec("Grapes")
            self.loadtbl()
    
    def loadtbl(self):
        people = sqlqueryselecttbl()
        column = ["Id", "Fruits",]
        
        self.mainui.tblFruits.clear()
        self.mainui.tblFruits.setColumnCount(len(column))
        self.mainui.tblFruits.setHorizontalHeaderLabels(column)
        self.mainui.tblFruits.setRowCount(len(people) - len(people))
        
        for person in people:
            inx = people.index(person)
            self.mainui.tblFruits.insertRow(inx)
            
            for st in range(2):
                self.mainui.tblFruits.setItem(inx, st, QTableWidgetItem(str(person[st])))
            self.mainui.tblFruits.update()

if __name__ == '__main__':
    app = QApplication([])
    main = Main()
    app.exec()